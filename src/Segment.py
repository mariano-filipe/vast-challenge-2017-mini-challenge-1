from datetime import datetime


class Segment:
    def __init__(self, in_gate, out_gate, in_time, out_time):
        self.in_gate = in_gate
        self.out_gate = out_gate
        self.in_time = in_time
        self.out_time = out_time
        FMT = '%Y-%m-%d %H:%M:%S'
        self.duration = datetime.strptime(
            out_time, FMT) - datetime.strptime(in_time, FMT)
