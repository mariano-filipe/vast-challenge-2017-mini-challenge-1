from Segment import Segment
from datetime import timedelta


class Route:
    def __init__(self):
        self.segments = []
        self.length = 0
        self.duration = timedelta()
        self.last_sensor_record = None

    def add_sensor_record(self, timestamp, gate_name):
        if self.last_sensor_record != None:
            in_time = self.last_sensor_record[0]
            in_gate = self.last_sensor_record[1]
            segment = Segment(in_gate, gate_name, in_time, timestamp)
            self.add_segment(segment)

        self.last_sensor_record = [timestamp, gate_name]

    def add_segment(self, segment):
        self.segments.append(segment)
        self.length += 1
        self.duration += segment.duration
