import pandas as pd
import numpy as np

from Car import Car

db_path = "./data/lekagul-sensor-data.csv"
sensor_records = pd.read_csv(db_path)

# Collecting cars and its routes (collection of segments)
cars = {}
for index, row in sensor_records.iterrows():
    car_id = row["car-id"]
    if car_id in cars:
        car = cars[car_id]
    else:
        car = Car(car_id, row["car-type"])
        cars[car_id] = car
    car.add_sensor_record(row["Timestamp"], row["gate-name"])

# Collecting sensors data
sensors = pd.read_csv("./data/sensor-pos-labeled.csv")

# Writing data to files
cars_file = open("./data/cars-data.csv", "w")
routes_file = open("./data/routes-data.csv", "w")
route_segments_file = open("./data/route-segments.csv", "w")
route_coordinates_file = open("./data/route-coordinates.csv", "w")
cars_file.write("id|type|no-routes\n")
routes_file.write("id|car-id|length|duration\n")
route_segments_file.write("route-id|gate-in|gate-out|time-in|time-out|duration\n")
route_coordinates_file.write("route-id|gate-name|date|x|y\n")
route_id = -1
for car_id in cars:
    cars_file.write("{}|{}|{}\n".format(car_id, cars[car_id].type, len(cars[car_id].routes)))
    for route in cars[car_id].routes:
        route_id += 1
        routes_file.write("{}|{}|{}|{}\n".format(route_id, car_id, route.length, route.duration.total_seconds()))
        for segment in route.segments:
            route_segments_file.write("{}|{}|{}|{}|{}|{}\n".format(route_id, segment.in_gate, segment.out_gate, segment.in_time, segment.out_time, segment.duration.total_seconds()))
            
            origin_gate_idx = np.where(sensors["label"] == segment.in_gate)[0]
            x_origin = sensors.iloc[origin_gate_idx]["x"].values[0]
            y_origin = sensors.iloc[origin_gate_idx]["y"].values[0]
            route_coordinates_file.write("{}|{}|{}|{}|{}\n".format(route_id, segment.in_gate, segment.in_time, x_origin, y_origin))
        destination_gate_idx = np.where(sensors["label"] == segment.out_gate)[0]
        x_destination = sensors.iloc[destination_gate_idx]["x"].values[0]
        y_destination = sensors.iloc[destination_gate_idx]["y"].values[0]
        route_coordinates_file.write("{}|{}|{}|{}|{}\n".format(route_id, segment.out_gate, segment.out_time, x_destination, y_destination))

cars_file.close()
routes_file.close()
route_segments_file.close()
route_coordinates_file.close()