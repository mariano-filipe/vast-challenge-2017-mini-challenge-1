from Route import Route


class Car:
    def __init__(self, _id, _type):
        self.id = _id
        self.type = _type
        self.routes = []

    def add_sensor_record(self, timestamp, gate_name):
        # (case 1) first route of all
        if len(self.routes) == 0:
            route = Route()
            self.routes.append(route)
        # (case 2) updating last route
        else:
            route = self.routes[-1]
            if (route.length > 0) and ("entrance" in route.last_sensor_record[1]):
                route = Route()
                self.routes.append(route)

        # update route with new sensor record
        route.add_sensor_record(timestamp, gate_name)
