import pandas as pd
import numpy as np

segment_names = pd.read_csv("./data/segment-names.csv")
sensors = pd.read_csv("./data/sensor-pos-labeled.csv")
segments_file = open("./data/segments.csv", "w")

segments_file.write("origin-gestination,gate-name,segment-id,x,y\n")
for idx, segment in segment_names.iterrows():
    segment_id = segment["Segment"]
    gate_names = segment_id.split("_")
    origin_gate_name = gate_names[0]
    destination_gate_name = gate_names[1]
    origin_gate_idx = np.where(sensors["label"] == origin_gate_name)[0]
    x_origin = sensors.iloc[origin_gate_idx]["x"].values[0]
    y_origin = sensors.iloc[origin_gate_idx]["y"].values[0]
    destination_gate_idx = np.where(sensors["label"] == destination_gate_name)[0]
    x_destination = sensors.iloc[destination_gate_idx]["x"].values[0]
    y_destination = sensors.iloc[destination_gate_idx]["y"].values[0]

    segments_file.write("{},{},{},{},{}\n".format("Origin", origin_gate_name, segment_id, x_origin, y_origin))
    segments_file.write("{},{},{},{},{}\n".format("Destination", destination_gate_name, segment_id, x_destination, y_destination))

segments_file.close()