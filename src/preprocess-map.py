import numpy as np
from PIL import Image

colors = [
        (255, 255, 255, "road"),
        (76, 255, 0, "entrance"),
        (255, 216, 0, "ranger-stop"),
        (255, 0, 0, "gate"),
        (255, 106, 0, "camping"),
        (0, 255, 255, "general-gate"),
        (255, 0, 220, "ranger-base")
        ]
threshold = 2

img = Image.open("img/lekagul-roadways.png")

pixels = img.load()
map_data_file = open("data/map-data.csv", "w")
map_data_file.write("x,y,type\n")
sensor_pos_file = open("data/sensor-pos.csv", "w")
sensor_pos_file.write("x,y,type\n")

for i in range(img.size[0]):
    for j in range(img.size[1]):
        pix_color = pixels[i, j][0:3]
        match_counter = 0
        for c in colors:
            diff = np.abs(np.subtract(pix_color, c[0:3]))
            if diff[0] <= threshold and diff[1] <= threshold and diff[2] <= threshold:
                map_data_file.write("{},{},{}\n".format(i, 199-j, c[3]))
                if c[3] != "road":
                    sensor_pos_file.write("{},{},{}\n".format(i, 199-j, c[3]))
                match_counter += 1
        if match_counter > 1:
            print("ERROR: ambiguity in pixel ({}, {})".format(i, j))

map_data_file.close()
sensor_pos_file.close()